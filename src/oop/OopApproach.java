package oop;

import java.util.Arrays;
import java.util.List;

import oop.operations.AndOperation;
import oop.operations.BooleanOperation;
import oop.operations.OrOperation;

public class OopApproach {
	public static void main(String[] args) {

		List<BooleanOperation> operations = Arrays.asList(new AndOperation(), new OrOperation());

		// here we can iterate over the operations without even knowing which implementations are included in the list
		for( BooleanOperation operation: operations)
		{
			TruthTable table = new TruthTable( operation );
			table.printTable();
		}
	}
}
