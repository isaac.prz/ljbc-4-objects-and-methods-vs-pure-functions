package oop.operations;

/**
 * Interface thar represents a logical operation
 *
 */
public interface BooleanOperation {
	
	/**
	 * @param arg1
	 * @param arg2
	 * @return the result from the operation between the two parameters
	 */
	boolean operate(boolean arg1, boolean arg2);
	
	/**
	 * @return the operation name
	 */
	String getOperationName();
	
}
