package oop.operations;

public class OrOperation implements BooleanOperation {
	
	@Override
	public boolean operate(boolean arg1, boolean arg2) {
		return arg1 || arg2;
	}

	@Override
	public String getOperationName() {
		return "OR";
	}

}
