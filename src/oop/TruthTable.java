package oop;

import oop.operations.BooleanOperation;

public class TruthTable {

	private BooleanOperation operation;

	public TruthTable(BooleanOperation operation) {
		super();
		this.operation = operation;
	}

	public void printTable()
	{
		// with printf  you can use place holders and add them after wards as parameters
		// the "\n" represents a new line
		System.out.printf("  a   |  b   | a %s b \n", operation.getOperationName());
		System.out.printf(" %s | %s |  %s \n", true, true, operation.operate(true, true));
		System.out.printf(" %s | %s |  %s \n", true, false, operation.operate(true, false));
		System.out.printf(" %s | %s |  %s \n", false, true, operation.operate(false, true));
		System.out.printf(" %s | %s |  %s \n", false, false, operation.operate(false, false));
	}
}
