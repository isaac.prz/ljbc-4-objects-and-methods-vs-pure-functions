package funcional;

public class FunctionalApproach {

	public static void main(String[] args) {
		// first we generate the parameter combination in a 2 dimensional matrix: true-true, true-false, false-true, false-false
		boolean[][] params = generateParameterCombinations();
		// then we generate the AND results and print them
		printTable("AND", params, generateAndResults( params ));
		// then we generate the OR results and print them
		printTable("OR", params, generateOrResults( params ));
	}
	
	/**
	 * @return the standard parameter combination for the truth matrix
	 */
	static boolean[][] generateParameterCombinations()
	{
		 boolean[][] params = new boolean[4][2];
		 params[0][0]=true;
		 params[0][1]=true;
		 params[1][0]=true;
		 params[1][1]=false;
		 params[2][0]=false;
		 params[2][1]=true;
		 params[3][0]=false;
		 params[3][1]=false;
		 
		 return params;
	}

	/**
	 * @param params  4x2 array of booleans representing the 2 paramter combinations 
	 * @return one dimensional array with the results for the AND operation for the given params
	 */
	static boolean[] generateAndResults( boolean[][] params ) {
		boolean[] results = new boolean[4];
		for (int i = 0; i < params.length; i++) {
			results[i] = operateAnd(params[i][0], params[i][1]);
		}
		return results;
	}
	
	/**
	 * @param params  4x2 array of booleans representing the 2 paramter combinations 
	 * @return one dimensional array with the results for the OR operation for the given params
	 */
	static boolean[] generateOrResults( boolean[][] params )
	{
		boolean[] results = new boolean[4];
		for (int i = 0; i < params.length; i++) {
			results[i] = operateOr(params[i][0], params[i][1]);
		}
		return results;
	}
	

	/**
	 * @param arg1 first boolean argument
	 * @param arg2 second boolean argument
	 * @return operates AND for the given parameters
	 */
	static boolean operateAnd(boolean arg1, boolean arg2) {
		return arg1 && arg2;
	}
	
	/**
	 * @param arg1 first boolean argument
	 * @param arg2 second boolean argument
	 * @return operates AND for the given parameters
	 */
	static boolean operateOr(boolean arg1, boolean arg2) {
		return arg1 || arg2;
	}
	
	
	/**
	 * @param operationName this is the operation name
	 * @param params 4x2 array of booleans representing the 2 paramter combinations 
	 * @param restuls one dimensional array with 4 positions representing the results for the given params
	 */
	static void printTable(String operationName, boolean[][] params, boolean[] results)
	{
		// with printf  you can use place holders and add them after wards as parameters
		// the "\n" represents a new line
		System.out.printf("  a   |  b   | a %s b \n", operationName);
		System.out.printf(" %s | %s |  %s \n", params[0][0], params[0][1], results[0]);
		System.out.printf(" %s | %s |  %s \n", params[1][0], params[1][1], results[1]);
		System.out.printf(" %s | %s |  %s \n", params[2][0], params[2][1], results[2]);
		System.out.printf(" %s | %s |  %s \n", params[3][0], params[3][1], results[3]);
	}
}
