# Ljbc-4 objects and methods vs pure functions

Learn java by challenge lesson 4: learn what is an object and how to interact with them using their methods. Additionally differ them from pure functions, used mostly for non object oriented programming.


## Getting started

### Checkout this Git repository
 * clone the repository `git clone git@gitlab.com:isaac.prz/ljbc-4-objects-and-methods-vs-pure-functions.git`
 * import it as eclipse project 
 
### a bit of the theory

 _What is a function?_
 A function is a block of code that performs a task. It can be called and reused multiple times. You can pass information to a function and it can send information back.

``` 
	/**
	 * @param arg1 first boolean argument
	 * @param arg2 second boolean argument
	 * @return operates AND for the given parameters
	 */
	static boolean operateAnd(boolean arg1, boolean arg2) {
		return arg1 && arg2;
	} 
```


 
 
 _What is an object?_
 An object, in object-oriented programming (OOP), is an abstract data type created by a developer. It can include multiple properties and methods and may even contain other objects. In most programming languages, objects are defined as classes.
 
 * To create an object first we need a Class, which is its definition. 

``` 
/**
 * util class to count everytime you call the increment method
 */
 class Counter
 {
 	int count=0;
 	void increment() { count=count+1; }
 	int getCount() { return count; }
 }
```

 * To create an object from a Class, you need to call new and assign it to a varible so you can use it.


``` 
  // create a new instance
  Counter counter = new Counter();
  for(int i=0;i<20;i++)
  {
    // to call a method of the class alwas use a dot inbetween
    counter.increment();
  }
  // exected to print the number 20
  System.out.print(counter.getCount());
```


 _What is a package?_
a package is a way to organize our code in folders, Important is, that every file in java that is place in a folder, needs to declare the name of the folder where you are just now in the first line: `package oop.operations;`. In case you need to use a Class from your own package you need to call it with an import: `import oop.operations.AndOperation;` 


Take a look in depth:

 * take a look about the typical theory of object oriented programming [classes/objects at geeksforgeeks](https://www.geeksforgeeks.org/classes-objects-java/) 
 * also check what is a pure function [in general at wikipedia](https://en.wikipedia.org/wiki/Pure_function) and in particular in java [pure-functions-in-java by dzone](https://dzone.com/articles/pure-bliss-with-pure-functions-in-java)
 
 no worries if you get a bit lost it will be clear in the examples
 
 
### our problem to solve
We want to print [the truth table](https://en.wikipedia.org/wiki/Truth_table) for different operations for 2 parameters and one result.

### The object oriented way
First of all there are may ways to solve this, this is only one proposal.

We create an interface that represent the operations we want to do.

 
```
package oop.operations;

/**
 * Interface thar represents a logical operation
 *
 */
public interface BooleanOperation {
	
	/**
	 * @param arg1
	 * @param arg2
	 * @return the result from the operation between the two parameters
	 */
	boolean operate(boolean arg1, boolean arg2);
	
	/**
	 * @return the operation name
	 */
	String getOperationName();
	
}


```

Then for every different operation we want to implement we add a new `class` that implements this interface, for instance `AndOperation` and  `OrOperation`

![Run OopApproach](docs/oop-approach.png)

### The fuctional way

pure fuctions are methods that for the same paramters return always the same results. In this case we decided to operate with boolean 2 dimensional arrays.

for instance the next function will always return the same result for the same parameters:

```
	/**
	 * @param params  4x2 array of booleans representing the 2 paramter combinations 
	 * @return one dimensional array with the results for the OR operation for the given params
	 */
	static boolean[] generateOrResults( boolean[][] params )
	{
		boolean[] results = new boolean[4];
		for (int i = 0; i < params.length; i++) {
			results[i] = params[i][0] || params[i][1];
		}
		return results;
	}
```
in a program with  functional paradigm (it's the standard for languages like C), the code should be distributed in such kind of functions. 

![Run FunctionalApproach](docs/functional-approach.png)
 
 
## Challenges

1. shorten the FunctionalApproach.printTable(...) function with the help of an iterator (for instance `for`). 
1. Add a new truth table (in this case for XOR) for the OopApproach
1. Add a new truth table (in this case for XOR) for the FunctionalApproach
1. Do you know that you can view what is happening in runtime, it's called debuggin. Try to debug both solutions following [this debugging guide](https://www.wikihow.com/Debug-with-Eclipse)


## Overall Information about  _Learn java by challenge_
 This is a set of projects or challeges, ordered in order to serve as a lesson. Every lesson will progress you into your goal, learning java with focus on enterprise practises. 

## Contributing
 Feel free to contribute any commentary, constructive critique, or PR with proposed changes is wellcome

## Authors and acknowledgment
 * Isaac Perez (main author)

## License

Copyright 2021 Isaac Perez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

More information [MIT License](https://opensource.org/licenses/MIT)

